import pathlib
import pytest
import day24 as aoc

PUZZLE_DIR = pathlib.Path(__file__).parent


@pytest.fixture
def example():
    puzzle_input = (PUZZLE_DIR / "example.txt").read_text().strip()
    return puzzle_input


def test_parse_into_maze(example):
    maze = aoc.Maze(example)
    assert maze.start == (1, 1)
    assert maze.targets[(1, 1)] == 0
    assert maze.targets[(1, 3)] == 1


def test_run(example):
    """Test part 1 on example input."""
    maze = aoc.Maze(example)
    p1, p2 = maze.run()
    assert p1 == 14
    assert p2 == 20
