#!/usr/bin/env python3

import pathlib
import sys
import time
from collections import deque
from termcolor import colored


class Maze:
    def __init__(self, puzzle_input) -> None:
        self.open_spaces = set()
        self.targets = {}
        for r, line in enumerate(puzzle_input.strip().splitlines()):
            for c, val in enumerate(line):
                if val != "#":
                    self.open_spaces.add((r, c))
                    try:
                        target = int(val)
                        self.targets[(r, c)] = target
                        if target == 0:
                            self.start = (r, c)
                    except ValueError:
                        pass
        self.part1 = None
        self.part2 = None

    def run(self) -> int:
        initial = State(*self.start, set([0]), [])
        queue = deque([initial])
        seen = set()

        while queue:
            state = queue.popleft()
            if state in seen:
                continue
            seen.add(state)

            r, c, targets, steps = state.dump()
            if (r, c) in self.targets:
                targets = targets.union(set([self.targets[(r, c)]]))
                if len(targets) == len(self.targets):
                    if not self.part1:
                        self.part1 = [s for s in steps]
                    if (r, c) == self.start:
                        self.part2 = [s for s in steps]
                        return len(self.part1), len(self.part2)

            for dr, dc in [(0, 1), (0, -1), (1, 0), (-1, 0)]:
                rr, cc = r + dr, c + dc
                if (rr, cc) in self.open_spaces:
                    queue.append(State(rr, cc, targets, steps + [(rr, cc)]))

    def animate(self):
        if not self.part2:
            print(f"Unable to animate before solve")
            return

        rmax = max(r for r, _ in self.open_spaces) + 2
        cmax = max(c for _, c in self.open_spaces) + 2

        got = []

        for step in self.part2:
            print("\033[H")
            for r in range(rmax):
                for c in range(cmax):
                    if (r, c) == step:
                        print(colored("█", "green"), end="")
                        if (r, c) in self.targets:
                            got.append(self.targets[(r, c)])
                    elif (r, c) in self.targets:
                        if self.targets[(r, c)] in got:
                            print(
                                colored(self.targets[(r, c)], "red", "on_green"), end=""
                            )
                        else:
                            print(
                                colored(self.targets[(r, c)], "green", "on_white"),
                                end="",
                            )
                    elif (r, c) in self.open_spaces:
                        print(colored("█", "light_red"), end="")
                    else:
                        print(colored("█", "black"), end="")
                print()
            time.sleep(0.01)


class State:
    def __init__(self, r, c, targets, steps) -> None:
        self.r = r
        self.c = c
        self.targets = targets
        self.steps = steps

    def __hash__(self):
        return hash(f"{self.r}.{self.c}.{str(self.targets)}")

    def __eq__(self, other):
        return self.r == other.r and self.c == other.c and self.targets == other.targets

    def dump(self):
        return self.r, self.c, self.targets, self.steps


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    maze = Maze(puzzle_input)
    solution1, solution2 = maze.run()
    maze.animate()
    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
