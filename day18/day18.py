#!/usr/bin/env python3

import pathlib
import sys


def next_row(row):
    """Get next row from row input."""
    row = "." + row + "."
    return "".join(
        "^" if row[i : i + 3] in ["^^.", ".^^", "^..", "..^"] else "."
        for i in range(len(row) - 2)
    )


def run(row, num_rows):
    """Solve part 1 and 2."""
    cnt = 0
    for _ in range(num_rows):
        cnt += len(row.replace("^", ""))
        row = next_row(row)
    return cnt


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1 = run(puzzle_input, 40)
    solution2 = run(puzzle_input, 400000)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
