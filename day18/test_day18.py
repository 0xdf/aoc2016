import pytest
import day18 as aoc


@pytest.mark.parametrize(
    "row,next_row",
    [
        ("..^^.", ".^^^^"),
        (".^^^^", "^^..^"),
        (".^^.^.^^^^", "^^^...^..^"),
        ("^^^...^..^", "^.^^.^.^^."),
        ("^.^^.^.^^.", "..^^...^^^"),
        ("..^^...^^^", ".^^^^.^^.^"),
    ],
)
def test_next_row(row, next_row):
    assert aoc.next_row(row) == next_row


def test_part1():
    """Test part 1 on example input."""
    assert aoc.part1(".^^.^.^^^^", 10) == 38


@pytest.mark.skip(reason="Not implemented")
@pytest.mark.parametrize("puzzle_input,expected", [("", ""), ("", ""), ("", "")])
def test_part2(puzzle_input, expected):
    """Test part 2 on example input."""
    parsed_input = aoc.parse(puzzle_input)
    assert aoc.part2(parsed_input) == expected
