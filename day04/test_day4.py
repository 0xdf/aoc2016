import pytest
import day4 as aoc


@pytest.mark.parametrize(
    "pinput, name, sector, checksum, valid",
    [
        ("aaaaa-bbb-z-y-x-123[abxyz]", "aaaaa-bbb-z-y-x", 123, "abxyz", True),
        ("a-b-c-d-e-f-g-h-987[abcde]", "a-b-c-d-e-f-g-h", 987, "abcde", True),
        ("not-a-real-room-404[oarel]", "not-a-real-room", 404, "oarel", True),
        ("totally-real-room-200[decoy]", "totally-real-room", 200, "decoy", False),
    ],
)
def test_room(pinput, name, sector, checksum, valid):
    room = aoc.Room(pinput)

    assert room.name == name
    assert room.sector == sector
    assert room.checksum == checksum
    assert room.is_valid() == valid


def test_room_decrypt():
    line = "qzmt-zixmtkozy-ivhz-343[]"
    room = aoc.Room(line)

    assert room.decrypted_name() == "very encrypted name"


@pytest.mark.parametrize(
    "puzzle_input,expected",
    [
        (
            "aaaaa-bbb-z-y-x-123[abxyz]\na-b-c-d-e-f-g-h-987[abcde]\nnot-a-real-room-404[oarel]\ntotally-real-room-200[decoy]\n",
            1514,
        )
    ],
)
def test_part1(puzzle_input, expected):
    """Test part 1 on example input."""
    parsed_input = aoc.parse(puzzle_input)
    assert aoc.part1(parsed_input) == expected
