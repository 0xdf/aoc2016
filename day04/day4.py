#!/usr/bin/env python3

import pathlib
import sys
from collections import Counter
from string import ascii_lowercase as lc


class Room:
    def __init__(self, line: str) -> None:
        """
        create room from line:
        aaaaa-bbb-z-y-x-123[abxyz]
        """
        self.name, data = line.strip().rsplit("-", 1)
        sector, self.checksum = data.rstrip("]").rsplit("[", 1)
        self.sector = int(sector)

    def is_valid(self) -> bool:
        """Check if checksum is valid"""
        counter = Counter(self.name.replace("-", ""))
        counts = counter.most_common()
        sorted_counts = sorted(counts, key=lambda x: (-x[1], x[0]))
        checksum = "".join(x[0] for x in sorted_counts[:5])
        return checksum == self.checksum

    def decrypted_name(self) -> str:
        """Decrypt the name"""
        s = self.sector % 26
        lookup = str.maketrans(lc + "-", lc[s:] + lc[:s] + " ")
        return self.name.translate(lookup)


def parse(puzzle_input):
    """Parse input."""
    lines = puzzle_input.splitlines()
    rooms = [Room(r) for r in lines]
    return rooms


def part1(data):
    """Solve part 1."""
    return sum(r.sector for r in data if r.is_valid())


def part2(data):
    """Solve part 2."""
    valid_rooms = [r for r in data if r.is_valid]
    candidates = (
        f"{r.sector} [{r.decrypted_name()}]"
        for r in valid_rooms
        if "pole" in r.decrypted_name()
    )
    return next(candidates)


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    data = parse(puzzle_input)
    solution1 = part1(data)
    solution2 = part2(data)

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
