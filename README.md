# AOC2016

## Revival of Code 2023

Python discord is running a revival of code, looking at the 2016 Advent of Code, starting June 23 at 8pm Eastern time.

## Advent of Code

Original challenges are available here:

https://adventofcode.com/2016

## genday

### Background

`genday.sh` is my script to create a folder for the day, generate the template code, and fetch the puzzle input.

There are optoins for Python and Rust, though I plan to solve the 2016 AOC in Python.

I've updated the template code to do a more test-driven approach this year, using modified templates inspired from [this Real Python article](https://realpython.com/python-advent-of-code/#practicing-advent-of-code-day-1-2019).

### Usage

To run it, run `. genday.sh [day number]`. For example:

```
~/advent_of_code_2016$ . genday16.sh 1
~/advent_of_code_2016/day01$ ls
day1.py  input.txt  test_day1.py
```

The logged in session cookie must be either set as the `AOC_SESSION` environment variable, or be in a file in the same directory named `.session`.