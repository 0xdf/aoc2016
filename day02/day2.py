#!/usr/bin/env python3

import pathlib
import sys


def parse(puzzle_input):
    """Parse input."""
    return puzzle_input.strip().split("\n")


def part1(data):
    """Solve part 1."""
    step_map = {"U": (0, -1), "D": (0, 1), "L": (-1, 0), "R": (1, 0)}
    x, y = 1, 1
    code = ""
    for line in data:
        for c in line:
            x = max(min(x + step_map[c][0], 2), 0)
            y = max(min(y + step_map[c][1], 2), 0)
        code += f"{x + y * 3 + 1}"
    return code


def part2(data):
    """Solve part 2."""
    step_map = {"U": (0, 1), "D": (0, -1), "L": (-1, 0), "R": (1, 0)}
    code_map = {
        (0, 2): "1",
        (-1, 1): "2",
        (0, 1): "3",
        (1, 1): "4",
        (-2, 0): "5",
        (-1, 0): "6",
        (0, 0): "7",
        (1, 0): "8",
        (2, 0): "9",
        (-1, -1): "A",
        (0, -1): "B",
        (1, -1): "C",
        (0, -2): "D",
    }
    x, y = -2, 0
    code = ""
    for line in data:
        for c in line:
            x2 = x + step_map[c][0]
            y2 = y + step_map[c][1]
            if abs(x2) + abs(y2) <= 2:
                x, y = x2, y2
        code += code_map[(x, y)]
    return code


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    data = parse(puzzle_input)
    solution1 = part1(data)
    solution2 = part2(data)

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
