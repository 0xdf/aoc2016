import pathlib
import pytest
import day10 as aoc

PUZZLE_DIR = pathlib.Path(__file__).parent


@pytest.fixture
def example1():
    puzzle_input = (PUZZLE_DIR / "example1.txt").read_text().strip()
    return puzzle_input


@pytest.fixture
def puzzle():
    puzzle_input = (PUZZLE_DIR / "input.txt").read_text().strip()
    return puzzle_input


def test_parse(example1):
    bots = aoc.parse(example1)

    assert bots[2].chips == [2, 5]
    assert bots[1].chips == [3]
    assert bots[0].chips == []
    assert bots[2].low == 1
    assert bots[2].high == 0


@pytest.mark.parametrize("fixture", ["example1", "puzzle"])
def test_parse_assumptions(fixture, request):
    data = request.getfixturevalue(fixture)
    bots = aoc.parse(data)

    assert len([b for b in bots.values() if b.ready_to_pass]) == 1
    assert all(b.high is not None for b in bots.values())
    assert all(b.low is not None for b in bots.values())
    assert all(b.htype in ["bot", "output"] for b in bots.values())
    assert all(b.ltype in ["bot", "output"] for b in bots.values())
    assert all(b.low >= 0 for b in bots.value())
    assert all(b.high >= 0 for b in bots.value())


@pytest.mark.parametrize("target,expected", [([2, 5], 2), ([2, 3], 1), ([3, 5], 0)])
def test_part1(target, expected, example1):
    """Test part 1 on example input."""
    bots = aoc.parse(example1)
    assert aoc.part1(bots, target=target) == expected


@pytest.mark.skip(reason="Not implemented")
@pytest.mark.parametrize("puzzle_input,expected", [("", ""), ("", ""), ("", "")])
def test_part2(puzzle_input, expected):
    """Test part 2 on example input."""
    parsed_input = aoc.parse(puzzle_input)
    assert aoc.part2(parsed_input) == expected
