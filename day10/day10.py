#!/usr/bin/env python3

import math
import pathlib
import sys
from collections import defaultdict, deque
from typing import List


class Bot:
    def __init__(self) -> None:
        self.high: int = None
        self.low: int = None
        self.htype: str = None
        self.ltype: str = None
        self.chips: List[int] = []

    def get_chip(self, c: int) -> None:
        """Receive a chip."""
        if c not in self.chips:
            self.chips = sorted(self.chips + [c])

    @property
    def ready_to_pass(self) -> bool:
        """Checks if bot is ready to pass a chip, as in it has more than one."""
        return len(self.chips) > 1


def parse(puzzle_input):
    """Parse input."""
    bots = defaultdict(Bot)
    for line in puzzle_input.strip().splitlines():
        t = line.split(" ")
        match t[0]:
            case "value":
                chip, bot = map(int, [t[1], t[5]])
                bots[bot].get_chip(chip)
            case "bot":
                ltype, htype = t[5], t[10]
                bot, low, high = map(int, [t[1], t[6], t[11]])
                bots[bot].low = low
                bots[bot].high = high
                bots[bot].ltype = ltype
                bots[bot].htype = htype
    return bots


def part1(bots, target: List[int] = [17, 61]):
    """Solve part 1."""
    queue = deque(botnum for botnum, bot in bots.items() if bot.ready_to_pass)

    while queue:
        botnum = queue.popleft()
        bot = bots[botnum]
        if not bot.ready_to_pass:
            continue
        if bot.chips == target:
            return botnum
        if bot.ltype == "bot":
            bots[bot.low].get_chip(bot.chips[0])
            queue.append(bot.low)
        if bot.htype == "bot":
            bots[bot.high].get_chip(bot.chips[1])
            queue.append(bot.high)


def part2(bots):
    """Solve part 2."""
    queue = deque(botnum for botnum, bot in bots.items() if bot.ready_to_pass)
    outputs = [None] * 3

    while queue:
        botnum = queue.popleft()
        bot = bots[botnum]
        if not bot.ready_to_pass:
            continue
        if bot.ltype == "bot":
            bots[bot.low].get_chip(bot.chips[0])
            queue.append(bot.low)
        elif bot.low < 3:
            outputs[bot.low] = bot.chips[0]
        if bot.htype == "bot":
            bots[bot.high].get_chip(bot.chips[1])
            queue.append(bot.high)
        elif bot.high < 3:
            outputs[bot.high] = bot.chips[1]
        if all(o is not None for o in outputs):
            return math.prod(outputs)


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    data = parse(puzzle_input)
    solution1 = part1(data)
    solution2 = part2(data)

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
