#!/usr/bin/env python3

import pathlib
import sys
from sympy.ntheory.modular import solve_congruence


def parse(puzzle_input):
    """Parse input."""
    res = []
    for line in puzzle_input.splitlines():
        t = line.replace("#", "").rstrip(".").split(" ")
        n, s, p = map(int, (t[1], t[3], t[11]))
        r = ((-n - p) % s, s)
        res.append(r)
    return res


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    data = parse(puzzle_input)
    solution1 = solve_congruence(*data)[0]
    data.append((4, 11))
    solution2 = solve_congruence(*data)[0]

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
