import pathlib
import pytest
import day15 as aoc

PUZZLE_DIR = pathlib.Path(__file__).parent


@pytest.fixture
def example1():
    puzzle_input = (PUZZLE_DIR / "example1.txt").read_text().strip()
    return puzzle_input


def test_parse(example1):
    res = aoc.parse(example1)
    assert res == [(0, 5), (1, 2)]


def test_part1(example1):
    """Test part 1 on example input."""
    parsed_input = aoc.parse(example1)
    assert aoc.solve_congruence(*parsed_input)[0] == 5
