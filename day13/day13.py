#!/usr/bin/env python3

import pathlib
import sys
from collections import deque

steps = [(-1, 0), (1, 0), (0, 1), (0, -1)]


def is_valid_space(x, y, salt):
    """Check if given space is valid for a given salt."""
    if x < 0 or y < 0:
        return False
    num = x * x + 3 * x + 2 * x * y + y + y * y
    num += salt
    num_ones = len(f"{num:b}".replace("0", ""))
    return not bool(num_ones % 2)


def bfs(salt, target):
    """Solve challenge."""
    queue = deque([(1, 1, 0)])
    visited = set([(1, 1)])
    visited_in_less_than_50 = set([(1, 1)])

    while queue:
        x, y, t = queue.popleft()
        if (x, y) == target:
            return t, len(visited_in_less_than_50)

        for dx, dy in steps:
            xx, yy = x + dx, y + dy
            if (xx, yy) in visited:
                continue
            if is_valid_space(xx, yy, salt):
                queue.append((xx, yy, t + 1))
                visited.add((xx, yy))
                if t < 50:
                    visited_in_less_than_50.add((xx, yy))


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    solution1, solution2 = bfs(salt=int(puzzle_input), target=(31, 39))
    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
