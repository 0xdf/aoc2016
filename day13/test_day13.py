import pytest
import day13 as aoc


@pytest.mark.parametrize(
    "pos, salt, expected",
    [
        ((0, 0), 10, True),
        ((-1, 0), 10, False),
        ((5, -5), 10, False),
        ((5, 2), 10, False),
        ((9, 6), 10, False),
        ((6, 6), 10, True),
    ],
)
def test_is_valid_space(pos, salt, expected):
    assert aoc.is_valid_space(*pos, salt) == expected


def test_bfs():
    """Test part 1 on example input."""
    result, _ = aoc.bfs(10, (7, 4))
    assert result == 11
