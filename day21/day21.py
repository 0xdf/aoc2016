#!/usr/bin/env python3

import pathlib
import sys


class Password:
    def __init__(self, puzzle_input: str) -> None:
        """Initialize password object"""
        self.instructions = puzzle_input.strip().splitlines()

    def scramble(self, initial_password: str) -> str:
        """Scramble a password according to instructions."""
        hash = initial_password
        for inst in self.instructions:
            t = inst.split(" ")
            match " ".join(t[:2]):
                case "swap position":
                    hash = self.swap_position(hash, int(t[2]), int(t[5]))
                case "swap letter":
                    hash = self.swap_letter(hash, t[2], t[5])
                case "rotate left":
                    hash = self.rotate(hash, int(t[2]))
                case "rotate right":
                    hash = self.rotate(hash, int(t[2]), right=True)
                case "rotate based":
                    hash = self.rotate_by_letter(hash, t[6])
                case "reverse positions":
                    hash = self.reverse(hash, int(t[2]), int(t[4]))
                case "move position":
                    hash = self.move(hash, int(t[2]), int(t[5]))
        return hash

    def unscramble(self, initial_password: str) -> str:
        """Unscramble a password according to instructions."""
        hash = initial_password
        for inst in self.instructions[::-1]:
            t = inst.split(" ")
            match " ".join(t[:2]):
                case "swap position":
                    hash = self.swap_position(hash, int(t[2]), int(t[5]))
                case "swap letter":
                    hash = self.swap_letter(hash, t[2], t[5])
                case "rotate left":
                    hash = self.rotate(hash, int(t[2]), right=True)
                case "rotate right":
                    hash = self.rotate(hash, int(t[2]), right=False)
                case "rotate based":
                    for i in range(len(hash) + 2):
                        hh = self.rotate(hash, i)
                        if self.rotate_by_letter(hh, t[6]) == hash:
                            hash = hh
                            break
                case "reverse positions":
                    hash = self.reverse(hash, int(t[2]), int(t[4]))
                case "move position":
                    hash = self.move(hash, int(t[5]), int(t[2]))
        return hash

    @staticmethod
    def swap_position(h: str, x: int, y: int) -> str:
        """swap position X with position Y means that the
        letters at indexes X and Y (counting from 0) should
        be swapped."""
        x, y = sorted((x, y))
        return h[:x] + h[y] + h[x + 1 : y] + h[x] + h[y + 1 :]

    @staticmethod
    def swap_letter(h: str, x: str, y: str) -> str:
        """swap letter X with letter Y means that the letters
        X and Y should be swapped (regardless of where they appear
        in the string)."""
        x_i, y_i = h.index(x), h.index(y)
        return Password.swap_position(h, x_i, y_i)

    @staticmethod
    def rotate(h: str, x: int, right: bool = False) -> str:
        """rotate left/right X steps means that the whole string
        should be rotated; for example, one right rotation would
        turn abcd into dabc."""
        if right:
            x = len(h) - x
        return h[x:] + h[:x]

    @staticmethod
    def rotate_by_letter(h: str, x: str) -> str:
        """rotate based on position of letter X means that the
        whole string should be rotated to the right based on the
        index of letter X (counting from 0) as determined before
        this instruction does any rotations. Once the index is
        determined, rotate the string to the right one time, plus
        a number of times equal to that index, plus one additional
        time if the index was at least 4."""
        idx = h.index(x) + 1
        if idx >= 5:
            idx += 1
        return Password.rotate(h, idx, right=True)

    @staticmethod
    def reverse(h: str, x: int, y: int) -> str:
        """reverse positions X through Y means that the span of
        letters at indexes X through Y (including the letters at
        X and Y) should be reversed in order."""
        x, y = sorted((x, y))
        return h[:x] + h[x : y + 1][::-1] + h[y + 1 :]

    @staticmethod
    def move(h: str, x: int, y: int) -> str:
        """move position X to position Y means that the letter
        which is at index X should be removed from the string,
        then inserted such that it ends up at index Y."""
        h2 = h[:x] + h[x + 1 :]
        return h2[:y] + h[x] + h2[y:]


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    password = Password(puzzle_input)
    solution1 = password.scramble("abcdefgh")
    solution2 = password.unscramble("fbgdceah")

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
