import pytest
import day14 as aoc


@pytest.mark.parametrize(
    "str,hash",
    [
        ("abc18", "0034e0923cc38887a57bd7b1d4f953df"),
        ("abc39", "347dac6ee8eeea4652c7476d0f97bee5"),
        ("abc816", "3aeeeee1367614f3061d165a5fe3cac3"),
    ],
)
def test_cached_md5(str, hash):
    assert aoc.cached_md5(str) == hash


@pytest.mark.parametrize(
    "num,hash",
    [
        (0, "577571be4de9dcce85a041ba0410f29f"),
        (1, "eec80a0c92dc8a0777c619d9bb51e910"),
        (2, "16062ce768787384c81fe17a7a60c7e3"),
        (2016, "a107ff634856bb300138cac6568c0f24"),
    ],
)
def test_cached_md5(num, hash):
    assert aoc.cached_md5("abc0", num) == hash


@pytest.mark.parametrize(
    "str,res", [("asdasdaaaerty", "a"), ("qwerasdf", None), ("aaabbbccc", "a")]
)
def test_get_triple(str, res):
    assert aoc.get_triple(str) == res


@pytest.mark.parametrize(
    "idx,char,res", [(18, "8", False), (39, "e", True), (92, "9", True)]
)
def test_is_valid_key(idx, char, res):
    assert aoc.is_valid_key(idx, char, "abc") == res


def test_generate_keys():
    key_generator = aoc.generate_keys("abc")
    assert next(key_generator) == 39
    assert next(key_generator) == 92


def test_generate_keys2():
    key_generator = aoc.generate_keys("abc", 2016)
    assert next(key_generator) == 10


@pytest.mark.skip()
def test_part1():
    """Test part 1 on example input."""
    assert aoc.part1("abc") == 22728


def test_part2():
    """Test part 2 on example input."""
    assert aoc.part2("abc") == 22551
