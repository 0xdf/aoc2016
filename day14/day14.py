#!/usr/bin/env python3

import hashlib
import pathlib
import re
import sys
from functools import cache

triple_regex = re.compile(r"(.)\1\1")


@cache
def cached_md5(str, stretch=0):
    hash = hashlib.md5(str.encode()).hexdigest()
    for _ in range(stretch):
        hash = cached_md5(hash)
    return hash


def get_triple(str):
    res = triple_regex.findall(str)
    if res:
        return res[0]


def is_valid_key(idx, char, salt, stretch=0):
    for i in range(1000):
        if char * 5 in cached_md5(f"{salt}{idx + i + 1}", stretch=stretch):
            return True
    return False


def generate_keys(salt, stretch=0):
    i = 0
    while True:
        hash = cached_md5(f"{salt}{i}", stretch=stretch)
        if triple := get_triple(hash):
            if is_valid_key(i, triple, salt, stretch=stretch):
                yield i
        i += 1


def part1(salt):
    """Solve part 1."""
    key_gen = generate_keys(salt)
    for _ in range(63):
        next(key_gen)
    return next(key_gen)


def part2(salt):
    """Solve part 2."""
    key_gen = generate_keys(salt, stretch=2016)
    for _ in range(63):
        next(key_gen)
    return next(key_gen)


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    solution1 = part1(puzzle_input)
    solution2 = part2(puzzle_input)

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
