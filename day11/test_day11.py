import pathlib
import pytest
import day11 as aoc

# PUZZLE_DIR = pathlib.Path(__file__).parent

# @pytest.fixture
# def example1():
#    puzzle_input = (PUZZLE_DIR / "example1.txt").read_text().strip()
#    return puzzle_input


@pytest.mark.skip(reason="Not implemented")
@pytest.mark.parametrize("puzzle_input,expected", [("", ""), ("", ""), ("", "")])
def test_part1(puzzle_input, expected):
    """Test part 1 on example input."""
    parsed_input = aoc.parse(puzzle_input)
    assert aoc.part1(parsed_input) == expected


@pytest.mark.skip(reason="Not implemented")
@pytest.mark.parametrize("puzzle_input,expected", [("", ""), ("", ""), ("", "")])
def test_part2(puzzle_input, expected):
    """Test part 2 on example input."""
    parsed_input = aoc.parse(puzzle_input)
    assert aoc.part2(parsed_input) == expected
