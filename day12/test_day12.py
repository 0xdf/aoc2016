import pathlib
import pytest
import day12 as aoc

PUZZLE_DIR = pathlib.Path(__file__).parent


@pytest.fixture
def example1():
    puzzle_input = (PUZZLE_DIR / "example1.txt").read_text().strip()
    return puzzle_input


def test_create_comp(example1):
    comp = aoc.Computer(example1)

    assert comp.regs["a"] == 0
    assert comp.lines == example1.strip().splitlines()
    assert comp.ip == 0


def test_run_comp(example1):
    comp = aoc.Computer(example1)
    comp.run()

    assert comp.regs["a"] == 42


def test_get_value():
    comp = aoc.Computer("")
    comp.regs["a"] = 17

    assert comp.get_value("a") == 17
    assert comp.get_value("423") == 423
