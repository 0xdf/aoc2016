#!/usr/bin/env python3

import pathlib
import sys
from collections import defaultdict


class Computer:
    def __init__(self, puzzle_input: str) -> None:
        self.regs = defaultdict(int)
        self.lines = puzzle_input.strip().splitlines()
        self.ip = 0

    def run(self) -> None:
        """Run instructions."""
        while self.ip < len(self.lines):
            t = self.lines[self.ip].split(" ")
            match t[0]:
                case "cpy":
                    self.regs[t[2]] = self.get_value(t[1])
                case "inc":
                    self.regs[t[1]] += 1
                case "dec":
                    self.regs[t[1]] -= 1
                case "jnz":
                    if self.get_value(t[1]) != 0:
                        self.ip += self.get_value(t[2])
                        continue
            self.ip += 1

    def get_value(self, val) -> int:
        """Get a value either from a register or as a static int."""
        try:
            return int(val)
        except ValueError:
            return self.regs[val]


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    comp = Computer(puzzle_input)
    comp.run()
    solution1 = comp.regs["a"]
    comp2 = Computer(puzzle_input)
    comp2.regs["c"] = 1
    comp2.run()
    solution2 = comp2.regs["a"]

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
