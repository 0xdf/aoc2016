#!/usr/bin/env python3

import pathlib
import sys


def expand(s):
    """expand string based on dragon algo"""
    return f'{s}0{s[::-1].translate(str.maketrans("01","10"))}'


def num_twos(n):
    bits = f"{n:b}"
    return len(bits) - len(bits.rstrip("0"))


def checksum(s):
    """generate checksum based on dragon algo"""
    twos = num_twos(len(s))
    # return ''.join((str(1 - (sum(int(b) for b in g) % 2)) for g in zip(*(iter(s),) * pow(2, twos))))
    return "".join(
        (
            str(1 - (len("".join(g).replace("0", "")) % 2))
            for g in zip(*(iter(s),) * pow(2, twos))
        )
    )


def run(s, size):
    """Solve challenge."""
    while len(s) < size:
        s = expand(s)
    return checksum(s[:size])


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    solution1 = run(puzzle_input, 272)
    solution2 = run(puzzle_input, 35651584)

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
