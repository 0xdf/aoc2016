import pytest
import day16 as aoc


@pytest.mark.parametrize(
    "puzzle_input,expected",
    [
        ("1", "100"),
        ("11111", "11111000000"),
        ("111100001010", "1111000010100101011110000"),
    ],
)
def test_expand(puzzle_input, expected):
    assert aoc.expand(puzzle_input) == expected


def test_checksum():
    assert aoc.checksum("110010110100") == "100"


@pytest.mark.parametrize(
    "num,twos", [(1, 0), (2, 1), (6, 1), (4, 2), (12, 2), (64 * 3, 6)]
)
def test_num_twos(num, twos):
    assert aoc.num_twos(num) == twos


def test_part1():
    """Test part 1 on example input."""
    assert aoc.run("10000", 20) == "01100"
