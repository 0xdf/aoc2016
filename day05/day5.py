#!/usr/bin/env python3

import hashlib
import pathlib
import sys


def solve(data):
    """Solve the puzzle for the given input."""
    part1 = ""
    part2 = ["_"] * 8
    i = 0
    while "_" in part2:
        h = hashlib.md5(f"{data}{i}".encode()).hexdigest()
        if h.startswith("00000"):
            if len(part1) < 8:
                part1 += h[5]
            try:
                pos = int(h[5])
                if 0 <= pos <= 7 and part2[pos] == "_":
                    part2[pos] = h[6]
            except ValueError:
                pass
            print(f"\r{part1:<8} {''.join(part2)}", end="")
        i += 1
    print()
    return part1, "".join(part2)


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
