import day5 as aoc


def test_solver():
    """Test solve on example input."""
    puzzle_input = "abc"
    part1, part2 = aoc.solve(puzzle_input)
    assert part1 == "18f47a30"
    assert part2 == "05ace8e3"
