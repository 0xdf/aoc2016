import pathlib
import pytest
import day6 as aoc

PUZZLE_DIR = pathlib.Path(__file__).parent


@pytest.fixture
def example1():
    puzzle_input = (PUZZLE_DIR / "example1.txt").read_text().strip()
    return puzzle_input


def test_part1(example1):
    """Test part 1 on example input."""
    print(example1)
    parsed_input = aoc.parse(example1)
    assert aoc.part1(parsed_input) == "easter"


def test_part2(example1):
    """Test part 2 on example input."""
    parsed_input = aoc.parse(example1)
    assert aoc.part2(parsed_input) == "advent"
