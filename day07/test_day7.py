import pytest
import day7 as aoc


@pytest.mark.parametrize(
    "puzzle_input,expected",
    [
        ("abba[mnop]qrst", 1),
        ("abcd[bddb]xyyx", 0),
        ("aaaa[qwer]tyui", 0),
        ("ioxxoj[asdfgh]zxcvbn", 1),
    ],
)
def test_part1(puzzle_input, expected):
    """Test part 1 on example input."""
    parsed_input = aoc.parse(puzzle_input)
    assert aoc.part1(parsed_input) == expected


@pytest.mark.parametrize(
    "puzzle_input,expected",
    [("aba[bab]xyz", 1), ("xyx[xyx]xyx", 0), ("aaa[kek]eke", 1), ("zazbz[bzb]cdb", 1)],
)
def test_part2(puzzle_input, expected):
    """Test part 2 on example input."""
    parsed_input = aoc.parse(puzzle_input)
    assert aoc.part2(parsed_input) == expected
