#!/usr/bin/env python3

import pathlib
import regex as re
import sys

abba = re.compile(r"([a-z])(?!\1)([a-z])\2\1")


def has_abba(s):
    """Check if a string contains an abba."""
    return bool(abba.search(s))


aba = re.compile(r"([a-z])(?!\1)([a-z])\1")


def get_babs(s):
    """Return list of all the babs in a string."""
    return [f"{b}{a}{b}" for a, b in aba.findall(s, overlapped=True)]


def parse(puzzle_input):
    """Parse input."""
    lines = puzzle_input.splitlines()
    ips = []
    for line in lines:
        data = re.split(r"\[|\]", line)
        seq = data[::2]
        hyper = data[1::2]
        ips.append((seq, hyper))
    return ips


def part1(data):
    """Solve part 1."""
    count = 0
    for ip in data:
        if any(has_abba(seq) for seq in ip[0]) and not any(
            has_abba(seq) for seq in ip[1]
        ):
            count += 1
    return count


def part2(data):
    """Solve part 2."""
    count = 0
    for ip in data:
        babs = [babs for seq in ip[0] for babs in get_babs(seq)]
        if any(bab in hyper for bab in babs for hyper in ip[1]):
            count += 1
    return count


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    data = parse(puzzle_input)
    solution1 = part1(data)
    solution2 = part2(data)

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
