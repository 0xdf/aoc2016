import pytest
import day17 as aoc


@pytest.mark.parametrize(
    "salt,doors",
    [
        ("hijkl", "UDL"),
        ("hijklD", "ULR"),
        ("hijklDR", ""),
        ("hijklDU", "R"),
        ("hijklDUR", ""),
    ],
)
def test_doors_open(salt, doors):
    """Test doors open on example input."""
    assert aoc.doors_open(salt) == doors


@pytest.mark.parametrize(
    "salt,part1,part2",
    [
        ("ihgpwlah", "DDRRRD", 370),
        ("kglvqrro", "DDUDRLRRUDRD", 492),
        ("ulqzkmiv", "DRURDRUDDLLDLUURRDULRLDUUDDDRR", 830),
    ],
)
def test_run(salt, part1, part2):
    """Test part 1/2 on example input."""
    assert aoc.run(salt) == (part1, part2)
