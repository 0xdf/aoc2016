#!/usr/bin/env python3

import hashlib
import pathlib
import sys
from collections import deque


def doors_open(s):
    """Return a string from UDLR for which doors are open"""
    h = hashlib.md5(s.encode()).hexdigest()
    return "".join(dir for dir, res in zip("UDLR", h) if res in "bcdef")


def run(salt):
    """Solve part 1."""
    queue = deque([(0, 0, "")])
    part1, part2 = None, 0

    while queue:
        x, y, path = queue.popleft()
        if x == 3 and y == 3:
            if not part1:
                part1 = path
            if len(path) > part2:
                part2 = len(path)
            continue
        options = doors_open(salt + path)
        for option in options:
            dx, dy = {"U": (0, -1), "D": (0, 1), "L": (-1, 0), "R": (1, 0)}[option]
            xx, yy = x + dx, y + dy
            if 0 <= xx <= 3 and 0 <= yy <= 3:
                queue.append((xx, yy, path + option))

    return part1, part2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = run(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
