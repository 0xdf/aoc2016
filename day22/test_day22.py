import pathlib
import pytest
import day22 as aoc

PUZZLE_DIR = pathlib.Path(__file__).parent


@pytest.fixture
def example():
    puzzle_input = (PUZZLE_DIR / "example.txt").read_text().strip()
    return puzzle_input


def test_parse(example):
    nodes = aoc.parse(example)
    assert len(nodes) == len(example.strip().splitlines()) - 2
    assert all(isinstance(n, aoc.Node) for n in nodes)
    assert nodes[0].x == 0
    assert nodes[3].size == 92


def test_part1(example):
    """Test part 1 on example input."""
    nodes = aoc.parse(example)
    assert aoc.part1(nodes) == 11


@pytest.mark.skip(reason="Not implemented")
@pytest.mark.parametrize("puzzle_input,expected", [("", ""), ("", ""), ("", "")])
def test_part2(puzzle_input, expected):
    """Test part 2 on example input."""
    parsed_input = aoc.parse(puzzle_input)
    assert aoc.part2(parsed_input) == expected
