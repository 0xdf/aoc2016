#!/usr/bin/env python3

import pathlib
import re
import sys


class Node:
    def __init__(self, x, y, size, used, avail, per) -> None:
        self.x = x
        self.y = y
        self.size = size
        self.used = used
        self.avail = avail
        self.per = per

    @property
    def pos(self):
        return (self.x, self.y)

    def __eq__(self, other) -> bool:
        return (self.x, self.y) == (other.x, other.y)


node_re = re.compile(
    r"/dev/grid/node-x(\d+)-y(\d+)\s+(\d+)T\s+(\d+)T\s+(\d+)T\s+(\d+)%"
)


def parse(puzzle_input):
    """Parse input."""
    return [Node(*map(int, n)) for n in node_re.findall(puzzle_input)]


def part1(nodes):
    """Solve part 1."""
    cnt = 0
    for a in nodes:
        for b in nodes:
            if a.used > 0 and a != b and a.used <= b.avail:
                cnt += 1
    return cnt


def draw_map(nodes):
    """Draw map of nodes"""
    grid = {}
    xmax = max(n.x for n in nodes)
    ymax = max(n.y for n in nodes)
    leftmost_wall = xmax
    for n in nodes:
        if n.pos == (0, 0):
            grid[n.pos] = "S"
        elif n.pos == (xmax, 0):
            grid[n.pos] = "G"
        elif n.used == 0:
            grid[n.pos] = "_"
            empty = n.pos
        elif n.size > 100:
            grid[n.pos] = "#"
            leftmost_wall = min(n.x, leftmost_wall)
        else:
            grid[n.pos] = "."
    map_str = ""
    for r in range(ymax + 1):
        for c in range(xmax + 1):
            map_str += grid[(c, r)]
        map_str += "\n"
    print(map_str)
    return xmax, empty, leftmost_wall


def part2(nodes):
    """Solve part 2."""
    xmax, empty, leftmost_wall_x = draw_map(nodes)
    moves = 0
    moves += empty[0] - leftmost_wall_x + 1  # move left past wall
    moves += empty[1]  # move to the top
    moves += xmax - leftmost_wall_x  # move to the goal data
    moves += (xmax - 1) * 5  # move data next to our disk
    moves += 1  # copy data into our disk
    return moves


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    data = parse(puzzle_input)
    solution1 = part1(data)
    solution2 = part2(data)

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
