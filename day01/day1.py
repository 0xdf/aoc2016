#!/usr/bin/env python3

import pathlib
import sys


def parse(puzzle_input):
    """Parse input."""
    return puzzle_input.split(", ")


def step(x, y, orientation, instruction):
    """Take one step."""
    orientation_map = {0: (0, 1), 1: (1, 0), 2: (0, -1), 3: (-1, 0)}
    visited = set()
    match instruction[0]:
        case "R":
            orientation = (orientation + 1) % 4
        case "L":
            orientation = (orientation - 1) % 4
        case _:
            raise ValueError(f"Unknown instruction: {instruction}")
    move = orientation_map[orientation]
    for _ in range(int(instruction[1:])):
        x += move[0]
        y += move[1]
        visited.add((x, y))
    return x, y, orientation, visited


def part1(data):
    """Solve part 1."""
    orientation = 0
    x, y = 0, 0
    for instruction in data:
        x, y, orientation, _ = step(x, y, orientation, instruction)
    return abs(x) + abs(y)


def part2(data):
    """Solve part 2."""
    orientation = 0
    x, y = 0, 0
    visited = set()
    for instruction in data:
        x, y, orientation, current_visited = step(x, y, orientation, instruction)
        for v in current_visited:
            if v in visited:
                return abs(v[0]) + abs(v[1])
            visited.add(v)


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    data = parse(puzzle_input)
    solution1 = part1(data)
    solution2 = part2(data)

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
