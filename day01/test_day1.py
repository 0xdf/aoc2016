import pytest
import day1 as aoc


@pytest.mark.parametrize(
    "puzzle_input,expected", [("R2, L3", 5), ("R2, R2, R2", 2), ("R5, L5, R5, R3", 12)]
)
def test_part1(puzzle_input, expected):
    """Test part 1 on example input."""
    parsed_input = aoc.parse(puzzle_input)
    assert aoc.part1(parsed_input) == expected


@pytest.mark.parametrize("puzzle_input,expected", [("R8, R4, R4, R8", 4)])
def test_part2(puzzle_input, expected):
    """Test part 2 on example input."""
    parsed_input = aoc.parse(puzzle_input)
    assert aoc.part2(parsed_input) == expected
