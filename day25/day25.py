#!/usr/bin/env python3

import pathlib
import re
import sys
from collections import defaultdict


class Computer:
    def __init__(self, puzzle_input: str) -> None:
        self.regs = defaultdict(int)
        self.lines = puzzle_input.strip().splitlines()
        self.ip = 0
        self.output = []

    def run(self) -> None:
        while self.ip < len(self.lines):
            if self.process_mult_block():
                continue
            t = self.lines[self.ip].split(" ")
            match t[0]:
                case "cpy":
                    self.regs[t[2]] = self.get_value(t[1])
                case "inc":
                    self.regs[t[1]] += 1
                case "dec":
                    self.regs[t[1]] -= 1
                case "jnz":
                    if self.get_value(t[1]) != 0:
                        self.ip += self.get_value(t[2])
                        continue
                case "out":
                    val = self.get_value(t[1])
                    if val not in [0, 1]:
                        return False
                    if self.output and self.output[-1] == val:
                        return False
                    self.output.append(val)
                    if len(self.output) > 50:
                        return True
                case "tgl":
                    target = self.ip + self.get_value(t[1])
                    try:
                        tt = self.lines[target].split(" ")
                        match len(tt):
                            case 2:
                                tt[0] = "dec" if tt[0] == "inc" else "inc"
                            case 3:
                                tt[0] = "cpy" if tt[0] == "jnz" else "jnz"
                        self.lines[target] = " ".join(tt)
                    except IndexError:
                        pass
            self.ip += 1

    def get_value(self, val) -> int:
        try:
            return int(val)
        except ValueError:
            return self.regs[val]

    mult_block_re = re.compile(
        r"cpy (-?\w+) (-?\w+)\ninc (-?\w+)\ndec \2\njnz \2 (-?\w+)\ndec (-?\w+)\njnz \5 (-?\w+)"
    )

    def process_mult_block(self) -> bool:
        block = "\n".join(self.lines[self.ip : self.ip + 6])
        if not self.mult_block_re.match(block):
            return False
        args = self.mult_block_re.findall(block)[0]
        self.regs[args[2]] += self.get_value(args[0]) * self.get_value(args[4])
        self.regs[args[1]] = 0
        self.regs[args[4]] = 0
        self.ip += 6
        return True


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    a = 0
    while True:
        comp = Computer(puzzle_input)
        comp.regs["a"] = a
        if comp.run():
            print("\r                                \r", end="")
            break
        print(f"\r{a} doesn't work", end="")
        a += 1
    solution1 = a
    solution2 = "No challenge part 2 today!"

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
