from math import log, floor


def part2(num):
    """Solve part 2."""
    x = list(range(1, num + 1))
    while len(x) > 1:
        l = len(x)
        x = x[: l // 2] + x[l // 2 + 1 :]
        x = x[1:] + x[:1]
    return x[0]


for num in range(2, 100):
    threes = pow(3, floor(log(num - 1, 3)))
    if num <= 2 * threes:
        res = num - threes
    else:
        res = num - threes + num - 2 * threes
    print(f"{num:3} {part2(num):3} {threes} {res} {res == part2(num)}")
