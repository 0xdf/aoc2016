import pytest
import day19 as aoc


def test_part1():
    """Test part 1 on example input."""
    assert aoc.part1(5) == 3


def test_part2():
    """Test part 2 on example input."""
    assert aoc.part2(5) == 2
