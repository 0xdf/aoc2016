#!/usr/bin/env python3

import pathlib
import sys
from math import floor, log


def part1(num):
    """Solve part 1."""
    n = len(f"{num:b}") - 1
    l = num - pow(2, n)
    return 2 * l + 1


def part2(num):
    """Solve part 2."""
    threes = pow(3, floor(log(num - 1, 3)))
    if num <= 2 * threes:
        return num - threes
    return num - threes + num - 2 * threes


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    data = int(puzzle_input)
    solution1 = part1(data)
    solution2 = part2(data)

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
