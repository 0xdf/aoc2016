#!/usr/bin/env python3

import pathlib
import re
import sys
from collections import deque


def parse(puzzle_input):
    """Parse input."""
    return deque(puzzle_input)


def decompress(data: deque, recurse: bool = False) -> int:
    """Decompress a given queue."""
    total_len = 0
    while data:
        token = data.popleft()
        if token == "(":
            while not token.endswith(")"):
                token += data.popleft()
            num, repeat = map(int, token[1:-1].split("x"))
            repeated_str = "".join(data.popleft() for _ in range(num))
            if recurse and re.search(r"\(\d+x\d+\)", repeated_str):
                repeated_str_len = (
                    decompress(deque(repeated_str), recurse=True) * repeat
                )
            else:
                repeated_str_len = len(repeated_str) * repeat
            total_len += repeated_str_len
        else:
            total_len += 1
    return total_len


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    data = parse(puzzle_input)
    solution1 = decompress(data.copy())
    solution2 = decompress(data, recurse=True)

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
