import pytest
import day20 as aoc


@pytest.fixture
def example():
    puzzle_input = """5-8
0-2
4-7
"""
    return puzzle_input


def test_parse(example):
    assert aoc.parse(example) == [(5, 8), (0, 2), (4, 7)]


# def test_part1(example):
#     """Test part 1 on example input."""
#     parsed_input = aoc.parse(example)
#     assert aoc.part1(parsed_input) == 3

# def test_part2(example):
#     """Test part 2 on example input."""
#     parsed_input = aoc.parse(example)
#     assert aoc.part2(parsed_input, 9) == 2


def test_run(example):
    """Test part 1/2 on example input."""
    parsed_input = aoc.parse(example)
    s1, s2 = aoc.run(parsed_input, 9)
    assert s1 == 3
    assert s2 == 2
