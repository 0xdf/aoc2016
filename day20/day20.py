#!/usr/bin/env python3

import pathlib
import sys


def parse(puzzle_input):
    """Parse input."""
    return [tuple(map(int, l.split("-"))) for l in puzzle_input.strip().splitlines()]


# def part1(data):
#     """Solve part 1."""
#     first, *starts = sorted(s for s, _ in data)
#     *ends, last = sorted(e for _, e in data)
#     if first != 0:
#         return 0
#     for s, e in zip(starts, ends):
#         if s > e + 1:
#             return e + 1

# def part2(data, max_val):
#     """Solve part 2."""
#     first, *starts = sorted(s for s, _ in data)
#     *ends, last = sorted(e for _, e in data)
#     cnt = first
#     for s, e in zip(starts, ends):
#         if s > e + 1:
#             cnt += s - e - 1
#     return cnt + max_val - last


def run(data, max_val):
    """Solve part 1/2."""
    first, *starts = sorted(s for s, _ in data)
    *ends, last = sorted(e for _, e in data)
    part1, part2 = None, first
    for s, e in zip(starts, ends):
        if s > e + 1:
            if not part1:
                part1 = e + 1
            part2 += s - e - 1
    return part1, part2 + max_val - last


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    data = parse(puzzle_input)
    # solution1 = part1(data)
    # solution2 = part2(data, 4294967295)
    solution1, solution2 = run(data, 4294967295)

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
