import pathlib
import pytest
import day8 as aoc

PUZZLE_DIR = pathlib.Path(__file__).parent


@pytest.fixture
def example1():
    puzzle_input = (PUZZLE_DIR / "example1.txt").read_text().strip()
    return puzzle_input


def test_parse(example1):
    screen = aoc.Screen(example1)
    assert screen.insts[0] == ("rect", 3, 2)
    assert screen.insts[1] == ("column", 1, 1)
    assert screen.insts[2] == ("row", 0, 4)
    assert screen.insts[3] == ("column", 1, 1)


def test_screen_init(example1):
    screen = aoc.Screen(example1, cols=7, rows=3)
    assert len(screen.screen) == 3
    assert len(screen.screen[0]) == 7
    assert screen.screen[2][4] == 0


def test_part1(example1):
    """Test part 1 on example input."""
    screen = aoc.Screen(example1, cols=7, rows=3)
    screen.run()
    assert screen.pixels_on() == 6


def test_part2(example1):
    """Test part 2 on example input."""
    screen = aoc.Screen(example1, cols=7, rows=3)
    screen.run()
    assert screen.to_string() == ".#..#.#\n#.#....\n.#....."
