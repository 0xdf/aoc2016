import pytest
import day3 as aoc


@pytest.mark.parametrize("puzzle_input,expected", [("5 10 25\n3  4 5\n6  7 9\n", 2)])
def test_part1(puzzle_input, expected):
    """Test part 1 on example input."""
    parsed_input = aoc.parse(puzzle_input)
    assert aoc.part1(parsed_input) == expected


@pytest.mark.parametrize("puzzle_input,expected", [("5 10 25\n3  4 5\n6  7 9\n", 2)])
def test_part2(puzzle_input, expected):
    """Test part 2 on example input."""
    parsed_input = aoc.parse(puzzle_input)
    assert aoc.part2(parsed_input) == expected
