#!/usr/bin/env python3

import pathlib
import sys


def is_valid_triangle(tri: tuple):
    """Check if a given tuple is a valid triangle."""
    a, b, c = tri
    return a + b > c and b + c > a and c + a > b


def parse(puzzle_input: str):
    """Parse input."""
    lines = puzzle_input.strip().split("\n")
    potential_triangles = [
        tuple(map(int, [x for x in l.split(" ") if x])) for l in lines
    ]
    return potential_triangles


def part1(data):
    """Solve part 1."""
    return len(list(filter(is_valid_triangle, data)))
    # return len([l for l in data if l[0] + l[1] > l[2] and l[1] + l[2] > l[0] and l[2] + l[0] > l[1]])


def part2(data):
    """Solve part 2."""
    tuples = list(zip(*(iter(data),) * 3))
    potential_triangles = []
    for t in tuples:
        for i in range(3):
            potential_triangles.append((t[0][i], t[1][i], t[2][i]))
    return len(list(filter(is_valid_triangle, potential_triangles)))


def solve(puzzle_input):
    """Solve the puzzle for the given input."""
    data = parse(puzzle_input)
    solution1 = part1(data)
    solution2 = part2(data)

    return solution1, solution2


if __name__ == "__main__":
    infile = (
        sys.argv[1]
        if len(sys.argv) > 1
        else pathlib.Path(__file__).parent / "input.txt"
    )
    puzzle_input = pathlib.Path(infile).read_text().strip()
    solution1, solution2 = solve(puzzle_input)
    if solution1:
        print(f" part1: {solution1}")
    if solution2:
        print(f" part2: {solution2}")
